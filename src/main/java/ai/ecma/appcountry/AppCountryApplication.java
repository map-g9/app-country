package ai.ecma.appcountry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppCountryApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppCountryApplication.class, args);
    }

}
